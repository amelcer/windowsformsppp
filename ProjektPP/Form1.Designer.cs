﻿namespace ProjektPP
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Grid_App = new System.Windows.Forms.TableLayoutPanel();
            this.Panel_navigation = new System.Windows.Forms.Panel();
            this.Grid_navigation = new System.Windows.Forms.TableLayoutPanel();
            this.Grid_workspace = new System.Windows.Forms.TableLayoutPanel();
            this.Label_header = new System.Windows.Forms.Label();
            this.Panel_results = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Label_result_variance = new System.Windows.Forms.Label();
            this.Label_result_minVal = new System.Windows.Forms.Label();
            this.Label_result_maxVal = new System.Windows.Forms.Label();
            this.Label_result_avg = new System.Windows.Forms.Label();
            this.Label_Avg = new System.Windows.Forms.Label();
            this.Label_maxVal = new System.Windows.Forms.Label();
            this.Label_minVal = new System.Windows.Forms.Label();
            this.Label_variance = new System.Windows.Forms.Label();
            this.label_results = new System.Windows.Forms.Label();
            this.Panel_samples = new System.Windows.Forms.Panel();
            this.Table_data = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Label_readedSamples = new System.Windows.Forms.Label();
            this.Panel_generateSamples = new System.Windows.Forms.Panel();
            this.Button_generateSamples = new System.Windows.Forms.Button();
            this.DivBar = new System.Windows.Forms.Panel();
            this.numericUpDown_numberOfSamples = new System.Windows.Forms.NumericUpDown();
            this.Label_generateSamples = new System.Windows.Forms.Label();
            this.Button_openFile = new System.Windows.Forms.Button();
            this.Button_export = new System.Windows.Forms.Button();
            this.Button_chart = new System.Windows.Forms.Button();
            this.Grid_App.SuspendLayout();
            this.Panel_navigation.SuspendLayout();
            this.Grid_navigation.SuspendLayout();
            this.Grid_workspace.SuspendLayout();
            this.Panel_results.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.Panel_samples.SuspendLayout();
            this.Panel_generateSamples.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_numberOfSamples)).BeginInit();
            this.SuspendLayout();
            // 
            // Grid_App
            // 
            this.Grid_App.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Grid_App.ColumnCount = 2;
            this.Grid_App.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.Grid_App.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.Grid_App.Controls.Add(this.Panel_navigation, 0, 0);
            this.Grid_App.Controls.Add(this.Grid_workspace, 1, 0);
            this.Grid_App.Location = new System.Drawing.Point(-5, -9);
            this.Grid_App.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Grid_App.Name = "Grid_App";
            this.Grid_App.RowCount = 1;
            this.Grid_App.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Grid_App.Size = new System.Drawing.Size(994, 847);
            this.Grid_App.TabIndex = 0;
            // 
            // Panel_navigation
            // 
            this.Panel_navigation.BackColor = System.Drawing.SystemColors.Highlight;
            this.Panel_navigation.Controls.Add(this.Grid_navigation);
            this.Panel_navigation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_navigation.Location = new System.Drawing.Point(4, 5);
            this.Panel_navigation.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Panel_navigation.Name = "Panel_navigation";
            this.Panel_navigation.Size = new System.Drawing.Size(91, 837);
            this.Panel_navigation.TabIndex = 0;
            // 
            // Grid_navigation
            // 
            this.Grid_navigation.BackColor = System.Drawing.Color.Transparent;
            this.Grid_navigation.ColumnCount = 1;
            this.Grid_navigation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Grid_navigation.Controls.Add(this.Button_openFile, 0, 0);
            this.Grid_navigation.Controls.Add(this.Button_export, 0, 2);
            this.Grid_navigation.Controls.Add(this.Button_chart, 0, 1);
            this.Grid_navigation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grid_navigation.Location = new System.Drawing.Point(0, 0);
            this.Grid_navigation.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Grid_navigation.Name = "Grid_navigation";
            this.Grid_navigation.RowCount = 3;
            this.Grid_navigation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 161F));
            this.Grid_navigation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 161F));
            this.Grid_navigation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 161F));
            this.Grid_navigation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.Grid_navigation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.Grid_navigation.Size = new System.Drawing.Size(91, 837);
            this.Grid_navigation.TabIndex = 0;
            // 
            // Grid_workspace
            // 
            this.Grid_workspace.ColumnCount = 1;
            this.Grid_workspace.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Grid_workspace.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.Grid_workspace.Controls.Add(this.Label_header, 0, 0);
            this.Grid_workspace.Controls.Add(this.Panel_results, 0, 2);
            this.Grid_workspace.Controls.Add(this.Panel_samples, 0, 1);
            this.Grid_workspace.Controls.Add(this.Panel_generateSamples, 0, 3);
            this.Grid_workspace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grid_workspace.Location = new System.Drawing.Point(102, 3);
            this.Grid_workspace.Name = "Grid_workspace";
            this.Grid_workspace.RowCount = 4;
            this.Grid_workspace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.Grid_workspace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.Grid_workspace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.Grid_workspace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.Grid_workspace.Size = new System.Drawing.Size(889, 841);
            this.Grid_workspace.TabIndex = 1;
            // 
            // Label_header
            // 
            this.Label_header.AutoSize = true;
            this.Label_header.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label_header.Font = new System.Drawing.Font("Sitka Display", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Label_header.Location = new System.Drawing.Point(3, 0);
            this.Label_header.Name = "Label_header";
            this.Label_header.Size = new System.Drawing.Size(883, 84);
            this.Label_header.TabIndex = 0;
            this.Label_header.Text = "Brak danych na których można pracować :(";
            this.Label_header.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Panel_results
            // 
            this.Panel_results.Controls.Add(this.panel1);
            this.Panel_results.Controls.Add(this.tableLayoutPanel1);
            this.Panel_results.Controls.Add(this.label_results);
            this.Panel_results.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_results.Location = new System.Drawing.Point(3, 339);
            this.Panel_results.Name = "Panel_results";
            this.Panel_results.Size = new System.Drawing.Size(883, 246);
            this.Panel_results.TabIndex = 1;
            this.Panel_results.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel1.Location = new System.Drawing.Point(10, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(857, 5);
            this.panel1.TabIndex = 3;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.Label_result_variance, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.Label_result_minVal, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.Label_result_maxVal, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.Label_result_avg, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Label_Avg, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.Label_maxVal, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Label_minVal, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.Label_variance, 0, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 75);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(471, 149);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // Label_result_variance
            // 
            this.Label_result_variance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Label_result_variance.AutoSize = true;
            this.Label_result_variance.Font = new System.Drawing.Font("Sitka Display", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Label_result_variance.Location = new System.Drawing.Point(238, 111);
            this.Label_result_variance.Name = "Label_result_variance";
            this.Label_result_variance.Size = new System.Drawing.Size(47, 38);
            this.Label_result_variance.TabIndex = 7;
            this.Label_result_variance.Text = "0,0";
            // 
            // Label_result_minVal
            // 
            this.Label_result_minVal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Label_result_minVal.AutoSize = true;
            this.Label_result_minVal.Font = new System.Drawing.Font("Sitka Display", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Label_result_minVal.Location = new System.Drawing.Point(238, 74);
            this.Label_result_minVal.Name = "Label_result_minVal";
            this.Label_result_minVal.Size = new System.Drawing.Size(47, 37);
            this.Label_result_minVal.TabIndex = 6;
            this.Label_result_minVal.Text = "0,0";
            // 
            // Label_result_maxVal
            // 
            this.Label_result_maxVal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Label_result_maxVal.AutoSize = true;
            this.Label_result_maxVal.Font = new System.Drawing.Font("Sitka Display", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Label_result_maxVal.Location = new System.Drawing.Point(238, 37);
            this.Label_result_maxVal.Name = "Label_result_maxVal";
            this.Label_result_maxVal.Size = new System.Drawing.Size(47, 37);
            this.Label_result_maxVal.TabIndex = 5;
            this.Label_result_maxVal.Text = "0,0";
            // 
            // Label_result_avg
            // 
            this.Label_result_avg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Label_result_avg.AutoSize = true;
            this.Label_result_avg.Font = new System.Drawing.Font("Sitka Display", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Label_result_avg.Location = new System.Drawing.Point(238, 0);
            this.Label_result_avg.Name = "Label_result_avg";
            this.Label_result_avg.Size = new System.Drawing.Size(47, 37);
            this.Label_result_avg.TabIndex = 4;
            this.Label_result_avg.Text = "0,0";
            // 
            // Label_Avg
            // 
            this.Label_Avg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_Avg.AutoSize = true;
            this.Label_Avg.Font = new System.Drawing.Font("Sitka Display", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Label_Avg.Location = new System.Drawing.Point(64, 0);
            this.Label_Avg.Name = "Label_Avg";
            this.Label_Avg.Size = new System.Drawing.Size(168, 37);
            this.Label_Avg.TabIndex = 0;
            this.Label_Avg.Text = "Wartość średnia";
            // 
            // Label_maxVal
            // 
            this.Label_maxVal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_maxVal.AutoSize = true;
            this.Label_maxVal.Font = new System.Drawing.Font("Sitka Display", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Label_maxVal.Location = new System.Drawing.Point(13, 37);
            this.Label_maxVal.Name = "Label_maxVal";
            this.Label_maxVal.Size = new System.Drawing.Size(219, 37);
            this.Label_maxVal.TabIndex = 1;
            this.Label_maxVal.Text = "Wartość maksymalna";
            // 
            // Label_minVal
            // 
            this.Label_minVal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_minVal.AutoSize = true;
            this.Label_minVal.Font = new System.Drawing.Font("Sitka Display", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Label_minVal.Location = new System.Drawing.Point(30, 74);
            this.Label_minVal.Name = "Label_minVal";
            this.Label_minVal.Size = new System.Drawing.Size(202, 37);
            this.Label_minVal.TabIndex = 2;
            this.Label_minVal.Text = "Wartość minimalna";
            // 
            // Label_variance
            // 
            this.Label_variance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_variance.AutoSize = true;
            this.Label_variance.Font = new System.Drawing.Font("Sitka Display", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Label_variance.Location = new System.Drawing.Point(120, 111);
            this.Label_variance.Name = "Label_variance";
            this.Label_variance.Size = new System.Drawing.Size(112, 38);
            this.Label_variance.TabIndex = 3;
            this.Label_variance.Text = "Wariancja";
            // 
            // label_results
            // 
            this.label_results.AutoSize = true;
            this.label_results.Font = new System.Drawing.Font("Sitka Display", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_results.Location = new System.Drawing.Point(3, 30);
            this.label_results.Name = "label_results";
            this.label_results.Size = new System.Drawing.Size(106, 42);
            this.label_results.TabIndex = 0;
            this.label_results.Text = "Wyniki:";
            this.label_results.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Panel_samples
            // 
            this.Panel_samples.Controls.Add(this.Table_data);
            this.Panel_samples.Controls.Add(this.panel2);
            this.Panel_samples.Controls.Add(this.Label_readedSamples);
            this.Panel_samples.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_samples.Location = new System.Drawing.Point(3, 87);
            this.Panel_samples.Name = "Panel_samples";
            this.Panel_samples.Size = new System.Drawing.Size(883, 246);
            this.Panel_samples.TabIndex = 2;
            // 
            // Table_data
            // 
            this.Table_data.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Table_data.ColumnCount = 1;
            this.Table_data.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Table_data.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.Table_data.Location = new System.Drawing.Point(10, 72);
            this.Table_data.Name = "Table_data";
            this.Table_data.RowCount = 1;
            this.Table_data.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Table_data.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.Table_data.Size = new System.Drawing.Size(856, 170);
            this.Table_data.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoSize = true;
            this.panel2.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel2.Location = new System.Drawing.Point(10, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(857, 5);
            this.panel2.TabIndex = 4;
            // 
            // Label_readedSamples
            // 
            this.Label_readedSamples.AutoSize = true;
            this.Label_readedSamples.Font = new System.Drawing.Font("Sitka Display", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Label_readedSamples.Location = new System.Drawing.Point(3, 29);
            this.Label_readedSamples.Name = "Label_readedSamples";
            this.Label_readedSamples.Size = new System.Drawing.Size(218, 42);
            this.Label_readedSamples.TabIndex = 3;
            this.Label_readedSamples.Text = "Wczytane próbki:";
            // 
            // Panel_generateSamples
            // 
            this.Panel_generateSamples.Controls.Add(this.Button_generateSamples);
            this.Panel_generateSamples.Controls.Add(this.DivBar);
            this.Panel_generateSamples.Controls.Add(this.numericUpDown_numberOfSamples);
            this.Panel_generateSamples.Controls.Add(this.Label_generateSamples);
            this.Panel_generateSamples.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_generateSamples.Location = new System.Drawing.Point(3, 591);
            this.Panel_generateSamples.Name = "Panel_generateSamples";
            this.Panel_generateSamples.Size = new System.Drawing.Size(883, 247);
            this.Panel_generateSamples.TabIndex = 3;
            this.Panel_generateSamples.Visible = false;
            // 
            // Button_generateSamples
            // 
            this.Button_generateSamples.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_generateSamples.ForeColor = System.Drawing.Color.Black;
            this.Button_generateSamples.Location = new System.Drawing.Point(423, 134);
            this.Button_generateSamples.Name = "Button_generateSamples";
            this.Button_generateSamples.Size = new System.Drawing.Size(75, 23);
            this.Button_generateSamples.TabIndex = 3;
            this.Button_generateSamples.Text = "Generuj";
            this.Button_generateSamples.UseVisualStyleBackColor = true;
            this.Button_generateSamples.Click += new System.EventHandler(this.Button_generateSamples_Click);
            // 
            // DivBar
            // 
            this.DivBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DivBar.AutoSize = true;
            this.DivBar.BackColor = System.Drawing.SystemColors.Highlight;
            this.DivBar.Location = new System.Drawing.Point(10, 15);
            this.DivBar.Name = "DivBar";
            this.DivBar.Size = new System.Drawing.Size(857, 5);
            this.DivBar.TabIndex = 2;
            // 
            // numericUpDown_numberOfSamples
            // 
            this.numericUpDown_numberOfSamples.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_numberOfSamples.Location = new System.Drawing.Point(435, 102);
            this.numericUpDown_numberOfSamples.MaximumSize = new System.Drawing.Size(46, 0);
            this.numericUpDown_numberOfSamples.Name = "numericUpDown_numberOfSamples";
            this.numericUpDown_numberOfSamples.Size = new System.Drawing.Size(46, 26);
            this.numericUpDown_numberOfSamples.TabIndex = 1;
            // 
            // Label_generateSamples
            // 
            this.Label_generateSamples.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_generateSamples.AutoSize = true;
            this.Label_generateSamples.Font = new System.Drawing.Font("Sitka Display", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Label_generateSamples.Location = new System.Drawing.Point(359, 57);
            this.Label_generateSamples.Name = "Label_generateSamples";
            this.Label_generateSamples.Size = new System.Drawing.Size(190, 42);
            this.Label_generateSamples.TabIndex = 0;
            this.Label_generateSamples.Text = "Generuj próbki";
            // 
            // Button_openFile
            // 
            this.Button_openFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Button_openFile.BackColor = System.Drawing.Color.Transparent;
            this.Button_openFile.FlatAppearance.BorderSize = 0;
            this.Button_openFile.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.HotTrack;
            this.Button_openFile.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.HotTrack;
            this.Button_openFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_openFile.Image = global::ProjektPP.Properties.Resources._006_file;
            this.Button_openFile.Location = new System.Drawing.Point(4, 5);
            this.Button_openFile.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Button_openFile.MaximumSize = new System.Drawing.Size(171, 207);
            this.Button_openFile.MinimumSize = new System.Drawing.Size(43, 51);
            this.Button_openFile.Name = "Button_openFile";
            this.Button_openFile.Size = new System.Drawing.Size(83, 151);
            this.Button_openFile.TabIndex = 3;
            this.Button_openFile.Tag = "Open";
            this.Button_openFile.Text = "Otwórz";
            this.Button_openFile.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.Button_openFile.UseVisualStyleBackColor = false;
            this.Button_openFile.Click += new System.EventHandler(this.Button_openFile_Click);
            // 
            // Button_export
            // 
            this.Button_export.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Button_export.BackColor = System.Drawing.Color.Transparent;
            this.Button_export.Enabled = false;
            this.Button_export.FlatAppearance.BorderSize = 0;
            this.Button_export.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.HotTrack;
            this.Button_export.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.HotTrack;
            this.Button_export.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_export.Image = global::ProjektPP.Properties.Resources._001_export;
            this.Button_export.Location = new System.Drawing.Point(4, 327);
            this.Button_export.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Button_export.MaximumSize = new System.Drawing.Size(171, 207);
            this.Button_export.MinimumSize = new System.Drawing.Size(43, 51);
            this.Button_export.Name = "Button_export";
            this.Button_export.Size = new System.Drawing.Size(83, 207);
            this.Button_export.TabIndex = 2;
            this.Button_export.Tag = "Wykres";
            this.Button_export.Text = "Eksportuj";
            this.Button_export.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.Button_export.UseVisualStyleBackColor = false;
            this.Button_export.Click += new System.EventHandler(this.Button_export_Click);
            // 
            // Button_chart
            // 
            this.Button_chart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Button_chart.BackColor = System.Drawing.Color.Transparent;
            this.Button_chart.Enabled = false;
            this.Button_chart.FlatAppearance.BorderSize = 0;
            this.Button_chart.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.HotTrack;
            this.Button_chart.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.HotTrack;
            this.Button_chart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_chart.Image = global::ProjektPP.Properties.Resources._003_report;
            this.Button_chart.Location = new System.Drawing.Point(4, 166);
            this.Button_chart.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Button_chart.MaximumSize = new System.Drawing.Size(171, 207);
            this.Button_chart.MinimumSize = new System.Drawing.Size(43, 51);
            this.Button_chart.Name = "Button_chart";
            this.Button_chart.Size = new System.Drawing.Size(83, 151);
            this.Button_chart.TabIndex = 1;
            this.Button_chart.Tag = "Wykres";
            this.Button_chart.Text = "Wykres";
            this.Button_chart.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.Button_chart.UseVisualStyleBackColor = false;
            this.Button_chart.Click += new System.EventHandler(this.Button_chart_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(979, 826);
            this.Controls.Add(this.Grid_App);
            this.Font = new System.Drawing.Font("Sitka Display", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(995, 865);
            this.Name = "Form1";
            this.Text = "Pomiary wartości";
            this.Grid_App.ResumeLayout(false);
            this.Panel_navigation.ResumeLayout(false);
            this.Grid_navigation.ResumeLayout(false);
            this.Grid_workspace.ResumeLayout(false);
            this.Grid_workspace.PerformLayout();
            this.Panel_results.ResumeLayout(false);
            this.Panel_results.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.Panel_samples.ResumeLayout(false);
            this.Panel_samples.PerformLayout();
            this.Panel_generateSamples.ResumeLayout(false);
            this.Panel_generateSamples.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_numberOfSamples)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel Grid_App;
        private System.Windows.Forms.Panel Panel_navigation;
        private System.Windows.Forms.TableLayoutPanel Grid_navigation;
        private System.Windows.Forms.Button Button_chart;
        private System.Windows.Forms.Button Button_export;
        private System.Windows.Forms.Button Button_openFile;
        private System.Windows.Forms.TableLayoutPanel Grid_workspace;
        private System.Windows.Forms.Label Label_header;
        private System.Windows.Forms.Panel Panel_results;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label Label_Avg;
        private System.Windows.Forms.Label Label_maxVal;
        private System.Windows.Forms.Label Label_minVal;
        private System.Windows.Forms.Label Label_variance;
        private System.Windows.Forms.Label Label_result_variance;
        private System.Windows.Forms.Label Label_result_minVal;
        private System.Windows.Forms.Label Label_result_maxVal;
        private System.Windows.Forms.Label Label_result_avg;
        private System.Windows.Forms.Panel Panel_samples;
        private System.Windows.Forms.Label Label_readedSamples;
        private System.Windows.Forms.Panel Panel_generateSamples;
        private System.Windows.Forms.NumericUpDown numericUpDown_numberOfSamples;
        private System.Windows.Forms.Label Label_generateSamples;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel DivBar;
        private System.Windows.Forms.Label label_results;
        private System.Windows.Forms.TableLayoutPanel Table_data;
        private System.Windows.Forms.Button Button_generateSamples;
    }
}

