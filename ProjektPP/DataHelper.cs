﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ProjektPP
{
    internal class DataHelper
    {
        public static void CalcResults(List<double> data)
        {
            List<double> sortedData = SortData(data);

            Values.MaxVal = sortedData[sortedData.Count - 1];
            Values.MinVal = sortedData[0];
            Values.AvgVal = GetAvg(sortedData);
            Values.VarianceVal = Variance(sortedData);

            LabelsHelper.PrintResults();
        }

        private static void MergeSort(List<double> data, int start, int end)
        {
            if (start != end)
            {
                int mid = (start + end) / 2;
                MergeSort(data, start, mid);
                MergeSort(data, mid + 1, end);
                Merge(data, start, mid, end);
            }
        }

        private static void Merge(List<double> data, int start, int mid, int end)
        {
            List<double> copyList = data.Select(item => item).ToList();

            int i = start, j = mid + 1, k = 0;

            while (i <= mid && j <= end)
            {
                if (data[j] < data[i])
                {
                    copyList[k] = data[j];
                    j++;
                }
                else
                {
                    copyList[k] = data[i];
                    i++;
                }
                k++;
            }

            if (i <= mid)
            {
                while (i <= mid)
                {
                    copyList[k] = data[i];
                    i++;
                    k++;
                }
            }
            else
            {
                while (j <= end)
                {
                    copyList[k] = data[j];
                    j++;
                    k++;
                }
            }

            for (i = 0; i <= end - start; i++)
                data[start + i] = copyList[i];
        }

        private static List<double> SortData(List<double> data)
        {
            List<double> arrayToSort = data.Select(item => item).ToList();
            MergeSort(arrayToSort, 0, arrayToSort.Count - 1);
            return arrayToSort;
        }

        private static double GetAvg(List<double> data)
        {
            double sum = 0;
            data.ForEach(x => sum += x);
            return sum / data.Count;
        }

        private static double Variance(List<double> data)
        {
            double avg = GetAvg(data);
            double sum = 0;
            data.ForEach(x => sum += Math.Pow(2, x - avg));

            return sum / data.Count;
        }

        public static List<double> GenerateSamples()
        {
            Random generator = new Random();
            NumericUpDown samplesInput = Program.mainForm.Controls.Find("numericUpDown_numberOfSamples", true).FirstOrDefault() as NumericUpDown;
            int samplesNumber = (int)samplesInput.Value;

            List<double> generatedSamples = new List<double>();

            for(int i = 0; i < samplesNumber; i++)
            {
                generatedSamples.Add(generator.NextDouble() * (Values.MaxVal - Values.MinVal) + Values.MinVal);
            }

            return generatedSamples;
        }
    }
}