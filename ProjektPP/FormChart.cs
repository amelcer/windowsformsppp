﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Drawing.Imaging;

namespace ProjektPP
{
    public partial class FormChart : Form
    {
        public static Color ChartMainColor { get; set; }
        public static Color ChartAvgColor { get; set; }
        public FormChart()
        {
            InitializeComponent();
        }

        private void button_chartColor_Click(object sender, EventArgs e)
        {
            colorDialog_main.ShowDialog();
            ChartMainColor = colorDialog_main.Color;
            button_chartColor.BackColor = ChartMainColor;
            chart1.Series["Results"].Color = ChartMainColor;
        }

        private void button_avgColor_Click(object sender, EventArgs e)
        {
            colorDialog_avg.ShowDialog();
            ChartAvgColor = colorDialog_avg.Color;
            button_avgColor.BackColor = ChartAvgColor;
            chart1.Series["AVG"].Color = ChartAvgColor;
        }

        private void FormChart_Load(object sender, EventArgs e)
        {
            chart1.Series["Results"].Points.DataBindXY(Enumerable.Range(1, Values.data.Count).ToList(), Values.data);
            
            
        }

        private void checkBox_displayLegend_CheckedChanged(object sender, EventArgs e)
        {
            Legend legend = chart1.Legends[0];
            if (checkBox_displayLegend.Checked)
                legend.Enabled = true;
            else
                legend.Enabled = false;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\wykres.png";
            ImageFormat format = ImageFormat.Png;
            chart1.SaveImage(path, format);

            MessageBox.Show("Wykres zapisany na pulpicie");
        }
    }
}
