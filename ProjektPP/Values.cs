﻿using System.Collections.Generic;

namespace ProjektPP
{
    public class Values
    {
        public static List<double> data { get; set; }
        public static double MaxVal { get; set; }
        public static double MinVal { get; set; }
        public static double VarianceVal { get; set; }
        public static double AvgVal { get; set; }
    }
}