﻿namespace ProjektPP
{
    partial class FormChart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormChart));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_avgColor = new System.Windows.Forms.Button();
            this.button_chartColor = new System.Windows.Forms.Button();
            this.checkBox_AvgValue = new System.Windows.Forms.CheckBox();
            this.checkBox_displayLegend = new System.Windows.Forms.CheckBox();
            this.colorDialog_main = new System.Windows.Forms.ColorDialog();
            this.colorDialog_avg = new System.Windows.Forms.ColorDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.chart1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(684, 566);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // chart1
            // 
            this.chart1.BackColor = System.Drawing.SystemColors.Control;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(3, 3);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Legend = "Legend1";
            series1.Name = "Results";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series2.Legend = "Legend1";
            series2.Name = "AVG";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(678, 390);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.button_avgColor);
            this.panel1.Controls.Add(this.button_chartColor);
            this.panel1.Controls.Add(this.checkBox_AvgValue);
            this.panel1.Controls.Add(this.checkBox_displayLegend);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 399);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(20);
            this.panel1.Size = new System.Drawing.Size(678, 164);
            this.panel1.TabIndex = 1;
            // 
            // button_avgColor
            // 
            this.button_avgColor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_avgColor.BackColor = System.Drawing.Color.Orange;
            this.button_avgColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_avgColor.Location = new System.Drawing.Point(571, 46);
            this.button_avgColor.Name = "button_avgColor";
            this.button_avgColor.Size = new System.Drawing.Size(84, 23);
            this.button_avgColor.TabIndex = 3;
            this.button_avgColor.Text = "Kolor średniej";
            this.button_avgColor.UseVisualStyleBackColor = false;
            this.button_avgColor.Click += new System.EventHandler(this.button_avgColor_Click);
            // 
            // button_chartColor
            // 
            this.button_chartColor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_chartColor.BackColor = System.Drawing.SystemColors.Highlight;
            this.button_chartColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_chartColor.Location = new System.Drawing.Point(571, 17);
            this.button_chartColor.Name = "button_chartColor";
            this.button_chartColor.Size = new System.Drawing.Size(84, 23);
            this.button_chartColor.TabIndex = 2;
            this.button_chartColor.Text = "Kolor wykresu";
            this.button_chartColor.UseVisualStyleBackColor = false;
            this.button_chartColor.Click += new System.EventHandler(this.button_chartColor_Click);
            // 
            // checkBox_AvgValue
            // 
            this.checkBox_AvgValue.AutoSize = true;
            this.checkBox_AvgValue.Checked = true;
            this.checkBox_AvgValue.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_AvgValue.Location = new System.Drawing.Point(7, 46);
            this.checkBox_AvgValue.Name = "checkBox_AvgValue";
            this.checkBox_AvgValue.Size = new System.Drawing.Size(103, 17);
            this.checkBox_AvgValue.TabIndex = 1;
            this.checkBox_AvgValue.Text = "Wartość średnia";
            this.checkBox_AvgValue.UseVisualStyleBackColor = true;
            // 
            // checkBox_displayLegend
            // 
            this.checkBox_displayLegend.AutoSize = true;
            this.checkBox_displayLegend.Checked = true;
            this.checkBox_displayLegend.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_displayLegend.Location = new System.Drawing.Point(7, 23);
            this.checkBox_displayLegend.Name = "checkBox_displayLegend";
            this.checkBox_displayLegend.Size = new System.Drawing.Size(109, 17);
            this.checkBox_displayLegend.TabIndex = 0;
            this.checkBox_displayLegend.Text = "Wyświetl legendę";
            this.checkBox_displayLegend.UseVisualStyleBackColor = true;
            this.checkBox_displayLegend.CheckedChanged += new System.EventHandler(this.checkBox_displayLegend_CheckedChanged);
            // 
            // colorDialog_main
            // 
            this.colorDialog_main.AnyColor = true;
            this.colorDialog_main.Color = System.Drawing.SystemColors.Highlight;
            // 
            // colorDialog_avg
            // 
            this.colorDialog_avg.AnyColor = true;
            this.colorDialog_avg.Color = System.Drawing.Color.Orange;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(268, 91);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Zapisz wykres";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormChart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(684, 561);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(700, 600);
            this.MinimumSize = new System.Drawing.Size(700, 600);
            this.Name = "FormChart";
            this.Text = "FormChart";
            this.Load += new System.EventHandler(this.FormChart_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox checkBox_AvgValue;
        private System.Windows.Forms.CheckBox checkBox_displayLegend;
        private System.Windows.Forms.ColorDialog colorDialog_main;
        private System.Windows.Forms.Button button_avgColor;
        private System.Windows.Forms.Button button_chartColor;
        private System.Windows.Forms.ColorDialog colorDialog_avg;
        private System.Windows.Forms.Button button1;
    }
}