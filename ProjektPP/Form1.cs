﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ProjektPP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button_openFile_Click(object sender, EventArgs e)
        {
            Values.data = FilesHelper.ReadFile();
            if (Values.data.Count > 0)
            {
                ShowHiddenElements();
                DataHelper.CalcResults(Values.data);
            }
        }

        private void ShowHiddenElements()
        {
            this.Controls.Find("Button_chart", true).FirstOrDefault().Enabled = true;
            this.Controls.Find("Button_export", true).FirstOrDefault().Enabled = true;
            this.Controls.Find("Panel_results", true).FirstOrDefault().Visible = true;
            this.Controls.Find("Panel_generateSamples", true).FirstOrDefault().Visible = true;
        }

        private void Button_export_Click(object sender, EventArgs e)
        {
            FilesHelper.SaveDataToFile();
        }

        private void Button_generateSamples_Click(object sender, EventArgs e)
        {
            List<double> generatedSamples = DataHelper.GenerateSamples();
        }

        private void Button_chart_Click(object sender, EventArgs e)
        {
            Form chartForm = new FormChart();
            chartForm.ShowDialog();
        }
    }
}