﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ProjektPP
{
    public static class LabelsHelper
    {
        public static void SetHeader(string text)
        {
            Program.mainForm.Controls.Find("Label_header", true).FirstOrDefault().Text = text;
        }

        public static void WriteData(List<double> data, string header)
        {
            TableLayoutPanel table = Program.mainForm.Controls.Find("Table_data", true).FirstOrDefault() as TableLayoutPanel;
            int columns = data.Count / (data.Count / 4);
            int rows = data.Count / columns;
            if (rows * columns < data.Count)
                rows += 1;

            table.Controls.Clear();
            table.ColumnStyles.Clear();
            table.RowStyles.Clear();

            table.ColumnCount = columns;
            table.RowCount = rows;

            float columnWidth = 100 / columns;
            float rowsHeight = 100 / rows;

            for (int i = 0; i < columns; i++)
            {
                table.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, columnWidth));
            }

            for (int i = 0; i < rows; i++)
            {
                table.RowStyles.Add(new ColumnStyle(SizeType.Percent, rowsHeight));
            }

            int n = 0;

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    table.Controls.Add(new Label { Text = data[n].ToString(), Anchor = AnchorStyles.Left }, i, j);
                    n++;
                    if (n >= data.Count)
                        break;
                }
                if (n >= data.Count)
                    break;
            }
        }

        public static void PrintResults()
        {
            WriteMax();
            WriteMin();
            WriteAvg();
            WriteVariance();
        }

        private static string Precision()
        {
            return "#.0000";
        }

        public static void WriteMax()
        {
            Label label = Program.mainForm.Controls.Find("Label_result_maxVal", true).FirstOrDefault() as Label;
            label.Text = Values.MaxVal.ToString(Precision());
        }

        public static void WriteMin()
        {
            Label label = Program.mainForm.Controls.Find("Label_result_minVal", true).FirstOrDefault() as Label;
            label.Text = Values.MinVal.ToString(Precision());
        }

        public static void WriteAvg()
        {
            Label label = Program.mainForm.Controls.Find("Label_result_avg", true).FirstOrDefault() as Label;
            label.Text = Values.AvgVal.ToString(Precision());
        }

        public static void WriteVariance()
        {
            Label label = Program.mainForm.Controls.Find("Label_result_variance", true).FirstOrDefault() as Label;
            label.Text = Values.VarianceVal.ToString(Precision());
        }
    }
}