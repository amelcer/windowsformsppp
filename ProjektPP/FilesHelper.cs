﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace ProjektPP
{
    public static class FilesHelper
    {
        public static List<double> ReadFile()
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;
            List<string> data = new List<string>();

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                openFileDialog.Filter = "txt files (*.txt)|*.txt";
                openFileDialog.Title = "Otwórz plik z wynikami";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        filePath = openFileDialog.FileName;
                        string lines = (File.ReadAllText(filePath));
                        data = lines.Split(';').ToList();
                        string fileHeader = data[0];
                        if (data.Count > 0)
                        {
                            data.RemoveAt(0);
                            int lastItemIndex = data.Count - 1;
                            if (string.IsNullOrEmpty(data[lastItemIndex]) || string.IsNullOrWhiteSpace(data[lastItemIndex]))
                            {
                                data.RemoveAt(lastItemIndex);
                            }

                            List<double> result = data.Select(d => double.Parse(d)).ToList();
                            if (result.Count > 0)
                            {
                                string header = "Dane z pliku: " + Path.GetFileName(filePath);
                                LabelsHelper.SetHeader(header);
                                LabelsHelper.WriteData(result, fileHeader);
                                return result;
                            }
                        }
                        else
                        {
                            throw new Exception("Plik nie zawiera danych");
                        }
                    }
                    catch (Exception e)
                    {
                        string caption = "Problem z załdowaniem pliku";
                        MessageBox.Show(e.Message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            return new List<double>();
        }

        public static void SaveDataToFile()
        {
            Stream myStream;
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Filter = "txt files (*.txt)|*.txt";
            saveFileDialog.Title = "Zapisz wyniki";
            saveFileDialog.FilterIndex = 1;
            saveFileDialog.RestoreDirectory = true;

            string dataToSave = String.Format(
                                                $"Wartość średnia: {Values.AvgVal} \n" +
                                                $"Wartość maksymalna: {Values.MaxVal} \n" +
                                                $"Wartość minimalna: {Values.MinVal} \n" +
                                                $"Wariancja: {Values.VarianceVal}");

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog.OpenFile()) != null)
                {
                    StreamWriter writer = new StreamWriter(myStream);
                    writer.Write(dataToSave);
                    writer.Flush();
                    myStream.Position = 0;
                    myStream.Close();
                }
            }
        }
    }
}